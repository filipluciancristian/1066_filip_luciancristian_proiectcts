package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.Aeroport;
import clase.AvionMarfuri;
import clase.AvionPersoane;
import clase.Client;
import clase.ClientBuilder;
import clase.PlataBiletCash;
import interfete.INivelSecuritate;

import static org.mockito.Mockito.*;

public class LCF_TestSuit_Part2 {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test16_AvionPersoane() {
		Client c1 = new ClientBuilder().setCnp("1940502450035").build();
		Client c2 = new ClientBuilder().setCnp("5120101419692").build();
		Client c3 = new ClientBuilder().setCnp("6120101419692").build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3);
		
		AvionPersoane ap = new AvionPersoane();
		boolean verificareMinori = ap.minoriLaBord(listaClienti);
		
		assertTrue(verificareMinori);
	}

	@Test
	public void test17_AvionPersoane() {
		Client c1 = new ClientBuilder().setCnp("1940502450035").build();
		Client c2 = new ClientBuilder().setCnp("1940502450035").build();
		Client c3 = new ClientBuilder().setCnp("1940502450035").build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3);
		
		AvionPersoane ap = new AvionPersoane();
		boolean verificareMinori = ap.minoriLaBord(listaClienti);
		
		assertFalse(verificareMinori);
	}
	
	@Test
	public void test18_AvionPersoane() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 700);
		Aeroport ap = new Aeroport();
		
		assertEquals("Avion marfa", ap.returnareTipAvion(am.codAvion));
	}
	
	@Test
	public void test19_AvionPersoane() {
		AvionMarfuri am = new AvionMarfuri("AP123", "Bucuresti", "Londra", 2000, 100, 1000, 700);
		Aeroport ap = new Aeroport();
		
		assertEquals("Avion persoane", ap.returnareTipAvion(am.codAvion));
	}
	
	@Test
	public void test20_AvionPersoane() {
		AvionMarfuri am = new AvionMarfuri("AC123", "Bucuresti", "Londra", 2000, 100, 1000, 700);
		Aeroport ap = new Aeroport();
		
		assertEquals("Neidentificat", ap.returnareTipAvion(am.codAvion));
	}
	
	@Test
	public void test21_AvionPersoane() {
		Client c1 = new ClientBuilder().setCnp("1440305049609").build();
		Client c2 = new ClientBuilder().setCnp("1940502450035").build();
		Client c3 = new ClientBuilder().setCnp("6120101419692").build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3);
		
		AvionPersoane ap = new AvionPersoane();
		int verificareBatrani = ap.batraniLaBord(listaClienti);
		
		assertEquals(1, verificareBatrani);
	}
	
	@Test
	public void test22_AvionPersoane() {
		Client c1 = new ClientBuilder().setCnp("1940502450035").build();
		Client c2 = new ClientBuilder().setCnp("1940502450035").build();
		Client c3 = new ClientBuilder().setCnp("6120101419692").build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3);
		
		AvionPersoane ap = new AvionPersoane();
		int verificareBatrani = ap.batraniLaBord(listaClienti);
		
		assertEquals(0, verificareBatrani);
	}
	
	@Test
	public void test23_AvionPersoane() {
		Client c1 = new ClientBuilder().setCnp("1940502450035").build();
		Client c2 = new ClientBuilder().setCnp("1940502450035").build();
		Client c3 = new ClientBuilder().setCnp("6120101419692").build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3);
		
		AvionPersoane ap = new AvionPersoane();
		
		int nrPasageri = ap.calculPasageri(listaClienti);
		
		assertFalse(ap.verificareLocuriInAvion(ap, nrPasageri));
	}
	
	@Test
	public void test24_AvionPersoane() {
		
		List<Client> listaClienti = new ArrayList<Client>();
		AvionPersoane ap = new AvionPersoane();
		int nrPasageri = ap.calculPasageri(listaClienti);
		
		assertTrue(ap.verificareLocuriInAvion(ap, nrPasageri));
	}
	
	@Test
	public void test25_Client_mockito1() {
		Client c1 = mock(Client.class);
		Client c2 = mock(Client.class);
		when(c1.getCnp()).thenReturn("1940502450035");
		when(c2.getCnp()).thenReturn("6120101419692");
		
		List<Client> listaPasageri = new ArrayList<>();
		listaPasageri.add(c1); listaPasageri.add(c2);
		
		AvionPersoane ap = new AvionPersoane();
		int verificareBatrani = ap.batraniLaBord(listaPasageri);
		
		assertEquals(0, verificareBatrani);
	}
	
	@Test
	public void test26_Client_mockito2() {
	
		Client c1 = mock(Client.class);
		Client c2 = mock(Client.class);
		when(c1.getCnp()).thenReturn("1940502450035");
		when(c2.getCnp()).thenReturn("6120101419692");
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2);
		
		AvionPersoane ap = new AvionPersoane();
		boolean verificareMinori = ap.minoriLaBord(listaClienti);
		
		assertTrue(verificareMinori);
	}
	
	@Test
	public void test27_AvionPersoane_mockito3() {
		AvionPersoane ap = mock(AvionPersoane.class);
		
		when(ap.getDistanta()).thenReturn(1000);
		
		assertEquals(0, ap.numarEscaleAvionPersoane(ap));
	}
	
	@Test
	public void test28_AvionPersoane_mockito4() {
		AvionPersoane ap = new AvionPersoane();
		
		float nrOre = ap.escaleDupaNumarOreDeZbor(500, 1000);
		
		assertEquals(2, nrOre, 0.2);
	}
	
	// Test Cross Check
	@Test
	public void test29_AvionPersoane_mockito5() {
		AvionPersoane ap = new AvionPersoane();
		float nrOre = ap.escaleDupaNumarOreDeZbor(500, 1000);
		
		int vitezaMedie = 500;
		int distanta = 1000;
		float timp = distanta/vitezaMedie;
		assertEquals(nrOre, timp, 0.2);
	}
}
