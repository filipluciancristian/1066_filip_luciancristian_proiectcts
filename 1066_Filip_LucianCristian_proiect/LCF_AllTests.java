package teste;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LCF_TestDesignPatterns.class, LCF_TestSuit_Part1.class, 
	LCF_TestSuit_Part2.class })
public class LCF_AllTests {

}
