package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.Client;
import clase.ClientBuilder;
import clase.FactoryAvion;

public class LCF_TestDesignPatterns {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		FactoryAvion factory;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		FactoryAvion factory = FactoryAvion.getInstanta("Fabrica3", "Calarasi");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1_SingleTon() {
		FactoryAvion factory1 = FactoryAvion.getInstanta("Fabrica1", "Bucuresti");
		FactoryAvion factory = FactoryAvion.getInstanta("Fabrica2", "Constanta");
		
		assertEquals(factory1.toString(), factory.toString());
	}

	@Test
	public void test2_Builder() {
		Client c1 = new ClientBuilder().setCnp("123456789").build();
		String cnpAsteptat = "123456789";
		
		assertEquals(cnpAsteptat, c1.cnp.toString());
	}
	
	
	
}
