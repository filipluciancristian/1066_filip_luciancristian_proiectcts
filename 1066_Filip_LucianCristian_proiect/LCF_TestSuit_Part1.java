package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.AvionMarfuri;
import clase.AvionPersoane;
import clase.Client;
import clase.ClientBuilder;

public class LCF_TestSuit_Part1 {
	static AvionMarfuri am;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		 am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 700);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 700);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test1_AvionMarfuri() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 700);
		int rezultatFunctie = am.calculNecesarCombustibilAvionMarfuri(am, 2000, 100, (int) am.gradIncarcareKG);
		int rezultatCalculat = 400200;
		
		assertEquals(rezultatCalculat, rezultatFunctie);
	}
	
	
	@Test
	public void test2_AvionMarfuri() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, -100);
		int rezultatFunctie = am.calculNecesarCombustibilAvionMarfuri(am, 2000, 100, (int) am.gradIncarcareKG);
		int rezultatCalculat = -1;
		
		assertEquals(rezultatCalculat, rezultatFunctie);
	}
	
	@Test
	public void test3_AvionMarfuri() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 0);
		int rezultatFunctie = am.calculNecesarCombustibilAvionMarfuri(am, 2000, 100, (int) am.gradIncarcareKG);
		int rezultatCalculat = -1;
		
		assertEquals(rezultatCalculat, rezultatFunctie);
	}
	
	@Test
	public void test4_AvionMarfuri() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 10000);
		int rezultatFunctie = am.calculNecesarCombustibilAvionMarfuri(am, 2000, 100, (int) am.gradIncarcareKG);
		int rezultatCalculat = 400200;
		
		assertEquals(rezultatCalculat, rezultatFunctie);
	}
	
	@Test
	public void test5_AvionMarfuri() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 10000);
		int rezultatFunctieCombustibil = am.calculNecesarCombustibilAvionMarfuri(am, 2000, 100, (int) am.gradIncarcareKG);
		int rezultatFunctie = am.numarEscaleAvionMarfuri(am, rezultatFunctieCombustibil);
		int rezultatCalculat = -1;
		
		assertEquals(rezultatCalculat, rezultatFunctie);
	}
	
	@Test
	public void test6_AvionMarfuri() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000, 0);
		int rezultatFunctieCombustibil = am.calculNecesarCombustibilAvionMarfuri(am, 2000, 100, (int) am.gradIncarcareKG);
		int rezultatFunctie = am.numarEscaleAvionMarfuri(am, rezultatFunctieCombustibil);
		int rezultatCalculat = 0;
		
		assertEquals(rezultatCalculat, rezultatFunctie);
	}
	
	@Test
	public void test7_AvionMarfuri() {
		AvionMarfuri am = new AvionMarfuri("AM123", "Bucuresti", "Londra", 2000, 100, 1000000, 8000);
		int rezultatFunctieCombustibil = am.calculNecesarCombustibilAvionMarfuri(am, 2000, 100, (int) am.gradIncarcareKG);
		int rezultatFunctie = am.numarEscaleAvionMarfuri(am, rezultatFunctieCombustibil);
		int rezultatCalculat = 0;
		
		assertEquals(rezultatCalculat, rezultatFunctie);
	}
	
	@Test
	public void test8_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 100000);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		float rezulultatCalcul = ap.calculTarifBagaj(c1, 100, 15);
		float rezultatAsteptat = 500;
		
		assertEquals(rezultatAsteptat, rezulultatCalcul, 0.2);
	}
	
	@Test
	public void test9_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 100000);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		float rezultatCalculat = ap.calculTarifBagaj(c1, 100, -1);
		float rezultatAsteptat = -1;
		
		assertEquals(rezultatAsteptat, rezultatCalculat, 0.2);
	}
	
	@Test
	public void test10_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 100000);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		float rezultatCalculat = ap.calculTarifBagaj(c1, -2, 10);
		float rezultatAsteptat = -2;
		
		assertEquals(rezultatAsteptat, rezultatCalculat, 0.2);
	}
	
	@Test 
	public void test11_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 100000);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		float rezultatCalculat = ap.calculTarifBagaj(c1, -2, -1);
		float rezultatAsteptat = 0;
		
		assertEquals(rezultatAsteptat, rezultatCalculat, 0.2);
	}
	
	@Test
	public void test12_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 100000);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c2 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c3 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c4 = new ClientBuilder().setNrKgBagaj(20).build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3); listaClienti.add(c4);
		
		int xCalculat = ap.gradIncarcareCala(listaClienti, (int)ap.gradIncarcareKG);
		int xAsteptat = 0;
		
		assertEquals(xAsteptat, xCalculat);
	}
	
	@Test
	public void test13_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 100);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c2 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c3 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c4 = new ClientBuilder().setNrKgBagaj(20).build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3); listaClienti.add(c4);
		
		int xCalculat = ap.gradIncarcareCala(listaClienti, (int)ap.gradIncarcareKG);
		int xAsteptat = 2;
		
		assertEquals(xAsteptat, xCalculat);
	}
	
	@Test
	public void test14_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 0);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c2 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c3 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c4 = new ClientBuilder().setNrKgBagaj(20).build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3); listaClienti.add(c4);
		
		int xCalculat = ap.gradIncarcareCala(listaClienti, (int)ap.gradIncarcareKG);
		int xAsteptat = 3;
		
		assertEquals(xAsteptat, xCalculat);
	}
	
	@Test
	public void test15_AvionPersoane() {
		AvionPersoane ap = new AvionPersoane("AP443", "Bucuresti", "Viena", 700, 500, 500000, 200);
		Client c1 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c2 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c3 = new ClientBuilder().setNrKgBagaj(20).build();
		Client c4 = new ClientBuilder().setNrKgBagaj(20).build();
		
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti.add(c1); listaClienti.add(c2); listaClienti.add(c3); listaClienti.add(c4);
		
		int xCalculat = ap.gradIncarcareCala(listaClienti, (int)ap.gradIncarcareKG);
		int xAsteptat = 1;
		
		assertEquals(xAsteptat, xCalculat);
	}
}
